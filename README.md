# Week 2 Exercises

Note: I had tried to make Q3 more efficient by starting with the beginning and end of the line (x) and then generate a random point between. This however doesn't seem to work

2. Edit minimise.py and write a function that calls testFunction with x values between −2 < x < 5, in steps of 0.01. Add logic to this function, such that it returns a list of x values for minima that have been found.
A generic curve f(x) is illustrated in Figure 1. This curve has two minima. The new function within minimise.py should return the x-axis position of both of these minima.
A minima can be found by computing three points: a current point, a previous point and a point before the previous point.

3. Edit minimise.py and add a function that:
   - Picks three random x values within the range −2 < x < 5.
   - Selects the two lowest values of the three calculated.
   - Selects a new third value at a random x point between the two remaining values.
   - Continues until the difference between the three points or two remaining values is less than 0.001. The difference should be computed as the difference between the points along the x-axis, which is illustrated in Figure 3.
   - Returns the value of x for the resulting minima.