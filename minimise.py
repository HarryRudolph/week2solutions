import random

import testFunction
from testFunction import testFunction


def scanMinima(numberList):
    minima = []
    for i in range(1, len(numberList)): # start at element 1 as the -1th element of the list returns none. 
        if numberList[i] < numberList[i-1] and numberList[i] < numberList[i+1]:
            minima.append((i-200)/100)
    return minima

def randMinima(numberList, xLower, xUpper):

    values = [[xLower, testFunction(xLower)],
              [xUpper, testFunction(xUpper)]]


    counter = 0
    
    while (abs(values[1][0] - values[0][0]) > 0.001):
        newX = random.uniform(values[0][0], values[1][0])

        values.append([newX, testFunction(newX)])
        values = sorted(values, key = lambda y: y[1])
        values = values[:2]

    return(values[0][0])


        
            
def minimise():
    yValueList = []
    x = -2
    while x <= 5: 
        yValueList.append(testFunction(x))
        x += 0.01
        
    print("Scanning Solution: " +str(scanMinima(yValueList)))
    
    print("RandomFunction: " + str(randMinima(yValueList, -2, 5))) 


minimise()
